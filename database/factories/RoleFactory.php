<?php

use Illuminate\Support\Str;
use Faker\Generator as Faker;

$factory->define(App\Role::class, function (Faker $faker) {
    return [
        'name'          => 'admin',
        'display_name'  => 'Adminstrator'
    ];
});
