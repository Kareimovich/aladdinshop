<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_user      = Role::where('name', 'User')->first();
        $role_store     = Role::where('name', 'Store')->first();
        $role_megastore = Role::where('name', 'MegaStore')->first();
        $role_admin     = Role::where('name', 'Admin')->first();



        $user = new User();
        $user->role_id  = $role_user->id;
        $user->name = 'Mohy Elsharkawy';
        $user->email = 'user@example.com';
        $user->password = bcrypt('password'); // password
        $user->save();


        $user_store = new User();
        $user_store->role_id  = $role_store->id;
        $user_store->name = 'Mohy Elsharkawy';
        $user_store->email = 'store@example.com';
        $user_store->password = bcrypt('password'); // password
        $user_store->save();


        $user_megastore = new User();
        $user_megastore->role_id  = $role_megastore->id;
        $user_megastore->name = 'Mohy Elsharkawy';
        $user_megastore->email = 'megastore@example.com';
        $user_megastore->password = bcrypt('password'); // password
        $user_megastore->save();


        $admin = new User();
        $admin->role_id  = $role_admin->id;
        $admin->name = 'Mohy Elsharkawy';
        $admin->email = 'admin@example.com';
        $admin->password = bcrypt('password'); // password
        $admin->save();

       
    }
}
