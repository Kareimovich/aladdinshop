<?php

use Illuminate\Database\Seeder;
use App\Role;
class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_user = new Role();
        $role_user->name = 'User';
        $role_user->display_name = 'A normal User';
        $role_user->save();

        $role_store = new Role();
        $role_store->name = 'Store';
        $role_store->display_name = 'An Store';
        $role_store->save();

        $role_megastore = new Role();
        $role_megastore->name = 'MegaStore';
        $role_megastore->display_name = 'An MegaStore';
        $role_megastore->save();

        $role_admin = new Role();
        $role_admin->name = 'Admin';
        $role_admin->display_name = 'Adminstrator';
        $role_admin->save(); 
    }
}
