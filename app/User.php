<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Role;

class User extends Authenticatable
{
   
  
    use  HasApiTokens,Notifiable;

    public function roles(){
        return $this->belongsTo(Role::class, 'role_id');
    }

    public function passwordHistories()
    {
        return $this->hasMany('App\PasswordHistory');
    }

   

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','role_id', 'old_passwords'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'is_admin'      => 'boolean', // Will convarted to (Bool)
        'old_passwords' => 'array', // Will convarted to (Array)
    ];
 
  
}
