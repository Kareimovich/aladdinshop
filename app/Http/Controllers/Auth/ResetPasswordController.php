<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Contracts\Auth\Authenticatable;
use App\User;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    // protected function resetpasswords($user)
    // {
    //     $old_password = $user->password;
    //     $user->old_passwords =$old_password;
    //     $user->save();

    //     // $user = App\User::find($user); // This will find the right user
    //     // $user->old_password = $password; // This will save the old password
    // }

    protected function addOldPassword( User $user, Request $request)
    {
        $user = User::where('email', $request->only('email'))->first(); // This will find the right user
        return( $user);
 
        $user->old_password = $user->password; // This will save the old password
        $user->password = $request->password;
        $user->save();
    }
}
