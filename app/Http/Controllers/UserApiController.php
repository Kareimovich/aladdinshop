<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;

class UserApiController extends Controller
{
    public function logout(Request $request){
    
        if (Auth::check()) {
            Auth::user()->token()->revoke();
            return response()->json(['success' =>' logout_success'],200);
        }else{ 
            return response()->json(['error' =>' api.something_went_wrong'], 500);
        }
    }
}
